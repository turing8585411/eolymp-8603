import java.util.Scanner;

//TIP To <b>Run</b> code, press <shortcut actionId="Run"/> or
// click the <icon src="AllIcons.Actions.Execute"/> icon in the gutter.
public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();

        int a=n/100+n/10%10+n%10;
        int b=(n/100)*(n/10%10)*(n%10);

        System.out.println(a+" "+b);
    }
}